import React from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import { useHistory } from 'react-router-dom';
import Button from '@material-ui/core/Button';
// import MailIcon from '@material-ui/icons/Mail';
// import Home from './pages/Home';
// import About from './pages/About';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import Home from './Home';
import About from './About';
import Erreurs from './Erreurs';
import PowerSettingsNewIcon from '@material-ui/icons/PowerSettingsNew';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  menuButton2 : {
    // marginLeft: theme.spacing(0),
    marginLeft : '80%'
  },
  hide: {
    display: 'none',
  },
  hide2: {
    display: 'flex',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
}));

export default function App2() {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const history = useHistory();
  const handleClick = () => history.push('/');



  return (
    <>
    <Router>
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, open && classes.hide)}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap>
           Control App
          </Typography>
          {/* <Button variant="btn btn-danger" onClick={handleClick} left>LogOut</Button> */}
          <IconButton
            color="secondary"
            aria-label="logOut"
            onClick={handleClick}
            edge="end"
            className={clsx(classes.menuButton2, open && classes.hide2)}
          >
            <PowerSettingsNewIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
      <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="left"
        open={open}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.drawerHeader}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
          </IconButton>
        </div>
        <Divider />
        <List >
          {/* {['Home', 'About'].map((text, index) => (
            // <Link to="/">
            <ListItem button key={text}>
              <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
              <ListItemText primary={text} />
            </ListItem>
            // </Link>
          ))} */}
          {/* <ListItem button component={Link} to="/"> 
            <ListItemIcon><InboxIcon /></ListItemIcon>
            <ListItemText primary="Login"/>
          </ListItem> */}

        {/* <ListItem button component={Link} to="/Erreurs"> 
            {/* <ListItemIcon><InboxIcon /></ListItemIcon> */}
            {/* <ListItemText primary="Erreurs"/>
          </ListItem> */} 

          <ListItem button component={Link} to="/Home"> 
            {/* <ListItemIcon><InboxIcon /></ListItemIcon> */}
            <ListItemText primary="Home"/>
          </ListItem>

          <ListItem button component={Link} to="/About"> 
            {/* <ListItemIcon><InboxIcon /></ListItemIcon> */}
            <ListItemText primary="About"/>
          </ListItem>

          

        

        


          
          
          {/* <ListItem button key={"Home"}>
          {/* <Link to="/" > Home</Link> */}
          

      
         {/* <div className="main">
         <ul>
                 <li>
                     <Link to="/" > Home</Link>
                </li>
                <li>
                     <Link to="/About">  About</Link>
                 </li>
           </ul>
         </div> */}
        </List>
        <Divider />
        <List>
          {/* {['Home', 'About', 'Spam'].map((text, index) => (
            <ListItem button key={text}>
              <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
              <ListItemText primary={text} />
            </ListItem>
          ))} */}
        </List>
      </Drawer>
      <main
        className={clsx(classes.content, {
          [classes.contentShift]: open,
        })}
      >
        <div className={classes.drawerHeader} />
        <Route path="/Home" exact={true} component={Home}/>
        <Route path="/About" exact={true} component={About}/>
        {/* <Route path="/" exact={true} component={Login}/> */}
        
      </main>
    </div>
    </Router>
    </>
  );
}
