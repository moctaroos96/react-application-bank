import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button'
import { Typography } from '@material-ui/core';
import SaveIcon from '@material-ui/icons/Save';
import AddIcon from '@material-ui/icons/Add';
import  DetailsIcon from '@material-ui/icons/DetailsOutlined'
import { MDBContainer, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import AlarmIcon from '@material-ui/icons/Alarm';
import MoreVertIcon from '@material-ui/icons/MoreVert';

// import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import '@fortawesome/fontawesome-free/css/all.min.css'; 
import 'bootstrap-css-only/css/bootstrap.min.css'; 
import 'mdbreact/dist/css/mdb.css';
// import classes from '*.module.css';

const columns = [
  { id: 'date_operation', label: 'Date de operation', minWidth: 170 },
  { id: 'reference', label: 'Reference', minWidth: 100 },
  {
    id: 'type_operation',
    label: 'Type de operation',
    minWidth: 100,
    align: 'right',
    // format: (value) => value.toLocaleString('en-US'),
  },
  {
    id: 'date_controle',
    label: 'Date de controle',
    minWidth: 150,
    align: 'right',
    // format: (value) => value.toLocaleString('en-US'),
  },
  {
    id: 'exploitent',
    label: 'Exploitent',
    minWidth: 170,
    align: 'right',
    // format: (value) => value.toFixed(2),
  },
  {
    id: 'actions',
    label: 'Actions',
    minWidth: 170,
    align: 'right',
    // format: (value) => value.toFixed(2),
  },
];

function createData(date_operation, reference, type_operation, date_controle,exploitent,actions) {
  return { date_operation, reference, type_operation, date_controle,exploitent,actions };
}

const rows = [
  createData('21/03/2021', 'IN', '1324171354', '21/03/2021','IN', <Button  variant="outlined" color="primary" size='small'>Detail</Button>),
  createData('21/03/2021', 'CN', '1403500365', '21/03/2021','IN', <IconButton color="secondary" aria-label="add an alarm">
  <AlarmIcon />
</IconButton>),
  createData('22/03/2021', 'IT', '60483973', '21/03/2021', 'IN', <IconButton color="secondary" aria-label="add an alarm">
  <MoreVertIcon/>
</IconButton>),
  createData('21/03/2021', 'US', '327167434', '21/03/2021', 'IN', 'details'),
  createData('21/03/2021', 'CA', '37602103', '21/03/2021', 'IN', 'details'),
  createData('21/03/2021', 'AU', '25475400', '21/03/2021', 'IN', 'details'),
  createData('21/03/2021', 'DE', '83019200', '21/03/2021', 'IN' , 'details'),
  createData('23/03/2021', 'IE', '4857000', '21/03/2021', 'IN', 'details'),
  createData('21/03/2021', 'MX', '126577691', '21/03/2021', 'IN', 'details'),
  createData('21/03/2021', 'JP', '126317000', '21/03/2021', 'IN', 'details'),
  createData('21/03/2021', 'FR', '67022000', '21/03/2021', 'IN', 'details'),
  createData('19/03/2021', 'GB', '67545757', '21/03/2021', 'IN', 'details'),
  createData('21/03/2021', 'RU', '146793744', '21/03/2021', 'IN', 'details'),
  createData('21/03/2021', 'NG', '200962417', '21/03/2021', 'IN', 'details'),
  createData('17/03/2021', 'BR', '210147125', '21/03/2021', 'IN', 'details'),
];

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 440,
  },
  addButton : {
     marginTop: 15,
     marginLeft : "86%",
     margin : 15,
    // position : 'absolute',
    // right : "10px"
  },
  dialogWrapper : {
    padding : theme.spacing(2),
    position : "absolute",
    top : theme.spacing(2)
  },
  root_form : {
    '& > *': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
}));

export default function Home() {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const [open, setOpen] = React.useState(false);

  // const handleOpen = () => {
  //   setOpen(true);
  // };

  // const handleClose = () => {
  //   setOpen(false);
  // };
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
   
    <Paper className={classes.root}>
       {/* <div> */}
         {/* <div  className={classes.T}> */}
         
         <Typography align = "center" variant="h4" color="primary" > Table Erreurs </Typography>
        
         {/* </div> */}
         
         <Button 
          variant="outlined" 
          className={classes.addButton} 
          color="primary" 
          onClick={handleClickOpen}  
          startIcon={<AddIcon />}
       >
              Ajouter
        </Button>
        <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title" classes = {{ paper : classes.dialogWrapper}}>
        <DialogTitle id="form-dialog-title">Ajouter une Erreure</DialogTitle>
        <DialogContent>
          {/* <DialogContentText>
            To subscribe to this website, please enter your email address here. We will send updates
            occasionally.
          </DialogContentText> */}
          {/* <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Email Address"
            type="email"
            fullWidth
          /> */}
          <form className={classes.root_form} noValidate autoComplete="off">
            {/* <TextField id="standard-basic" label="Standard" />
            id="standard-full-width"
          label="Label"
          style={{ margin: 8 }}
          placeholder="Placeholder"
          helperText="Full width!"
          fullWidth
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }}
            <TextField id="filled-basic" label="Filled" variant="filled" /> */}
            <TextField 
            id="date-operation" 
            label="Date de operation"
            style={{ margin: 8 }}  
            type="date" 
            variant="outlined" 
            InputLabelProps={{
              shrink: true,
            }} 
          />
            <TextField id="reference" label="Reference" variant="outlined" />
            <TextField id="type-operation" label="Type de operation" variant="outlined" />
            <TextField id="date-controle" label="Date de controle" variant="outlined" style={{ margin: 8 }}  
            type="date" 
            InputLabelProps={{
              shrink: true,
            }} />
            <TextField id="exploitent" label="Exploitent" variant="outlined" />
            <TextField id="nom-exploitent" label="Nom de exploitent" variant="outlined" />
            <TextField id="code-agence" label="Code de agence" variant="outlined" />
            <TextField id="service" label="Service" variant="outlined" />
            <TextField id="description" label="Description" variant="outlined" />
            <TextField id="impact" label="Impact" variant="outlined" />
            <TextField id="action" label="Action" variant="outlined" />
            <TextField id="justificatif-action" label="Justificatif de action" variant="outlined" />
            <TextField id="commentaire" label="Commentaire" variant="outlined" />
            <TextField id="docs" label="Docs" variant="outlined" />
            {/* <TextField id="outlined-basic" label="Outlined" variant="outlined" /> */}
          </form>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="secondary" variant="contained">
            Annuler
          </Button>
          <Button onClick={handleClose} color="primary" variant="contained" startIcon={<SaveIcon />}>
            Ajouter
          </Button>
        </DialogActions>
      </Dialog>
           
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                  
                </TableCell>
                
              ))}
              {/* <TableCell align='left'>
                Date de oepration
              </TableCell>
              <TableCell align='left'>
                Reference
              </TableCell>
              <TableCell align='left'>
                Type de oepration
              </TableCell>
              <TableCell align='left'>
                Date de Controle
              </TableCell>
              <TableCell align='left'>
                Exploitent
              </TableCell>
              <TableCell align='left'>
                Actions
              </TableCell> */}
              {/* <TableCell align='right'>
                Date de oepration
              </TableCell>
              <TableCell align='right'>
                Date de oepration
              </TableCell>
              <TableCell align='right'>
                Date de oepration
              </TableCell>
              <TableCell align='right'>
                Date de oepration
              </TableCell>
              
              <TableCell align='right'>
                Date de oepration
              </TableCell>
              <TableCell align='right'>
                Date de oepration
              </TableCell>
              <TableCell align='right'>
                Date de oepration
              </TableCell>
               */}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
              return (
                <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                  {columns.map((column) => {
                    const value = row[column.id];
                    return (
                      <TableCell key={column.id} align={column.align}>
                        {column.format && typeof value === 'number' ? column.format(value) : value}
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
            {/* <TableCell>
              23/12/2021
            </TableCell>
            <TableCell>
              0021ZZ
            </TableCell>
            <TableCell>
              OPER001
            </TableCell>
            <TableCell>
              12/12/2022
            </TableCell> */}
            
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </Paper>
  );
}
